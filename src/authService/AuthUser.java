package authService;

import db.DB;
import entity.User;
import enums.Role;
import util.Input;

import java.util.Iterator;

public class AuthUser {
    public static User currentUser;

    static {
        User sampleUser = new User("123", "123", "123");
        sampleUser.setRole(Role.Admin);
        DB.USERS.add(sampleUser);
    }

    public static void displayMenu() {
        System.out.println("""
                1 - Login
                2 - Register""");
    }

    public static boolean promote() throws InterruptedException {
        if (AuthUser.currentUser.getRole().equals(Role.User)) {
            System.out.println("You are not allowed to change warehouse");
            Thread.sleep(700);
            System.out.println("Press 1 to request to become ADMIN, \n0 to continue");
            switch (Input.inputInt("Choose")) {
                case 1 -> {
                    DB.REQUESTED_USERS.add(AuthUser.currentUser);
                    Thread.sleep(500);
                    System.out.println("You will have access after Admins approval!");
                }
                case 0 -> {
                    System.out.println("Continuing with current role");
                }

            }
            return true;
        }
        return false;
    }

    public static void authenticate() throws InterruptedException {
        displayMenu();
        switch (Input.inputInt("choose:")) {
            case 1 -> loginUser();
            case 2 -> registerUser();
        }
    }

    public static void registerUser() {
        User user = new User(
                Input.inputStr("Enter your name:"),
                Input.inputStr("Enter your email:"),
                Input.inputStr("Enter your password:")
        );
        DB.USERS.add(user);
        System.out.println("You have been successfully registered!");
        System.out.println("Getting back to the login page");
    }


    private static void loginUser() throws InterruptedException {
        String email = Input.inputStr("Enter your email:");
        String password = Input.inputStr("Enter your password");
        for (User user : DB.USERS) {
            if (user.getEmail().equals(email) && user.getPassword().equals(password)) {
                currentUser = user;

                System.out.println("Successfully logged as a " + user.getRole() + " role");
                Thread.sleep(1000);
            }
        }
    }


    public static void assignAdmin() {
        if (DB.REQUESTED_USERS.isEmpty()) {
            System.out.println("There are no requests yet");
        } else {
            Iterator<User> iterator = DB.REQUESTED_USERS.iterator();
            while (iterator.hasNext()) {
                User requestedUser = iterator.next();
                if (requestedUser.getRole().equals(Role.Admin)) {
                    iterator.remove();
                }
            }


            for (int i = 0; i < DB.REQUESTED_USERS.size(); i++) {
                System.out.println(i + 1 + ". " + DB.REQUESTED_USERS.get(i).getName() +
                        ". Email: " + DB.REQUESTED_USERS.get(i).getEmail() +
                        ". Password " + DB.REQUESTED_USERS.get(i).getPassword());
            }

            int selectedUser = Input.inputInt("choose user") - 1;
            DB.REQUESTED_USERS.get(selectedUser).setRole(Role.Admin);
            System.out.println(DB.REQUESTED_USERS.get(selectedUser).getName() + " has access to Warehouse");
        }
    }
}

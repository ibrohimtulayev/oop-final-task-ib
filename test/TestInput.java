import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class TestInput {
    private static final Scanner scanner = new Scanner(System.in);

    private TestInput() {
        // private constructor to prevent instantiation
    }

    public static void provideInput(String input) {
        System.setIn(new ByteArrayInputStream(input.getBytes()));
    }

    public static int inputInt(String msg) {
        System.out.println(msg);
        return scanner.nextInt();
    }

    public static double inputDouble(String msg) {
        System.out.println(msg);
        return scanner.nextDouble();
    }

    public static String inputStr(String msg) {
        System.out.println(msg);
        scanner.nextLine();  // Consume the newline character left by nextDouble or nextInt
        return scanner.nextLine();
    }
}

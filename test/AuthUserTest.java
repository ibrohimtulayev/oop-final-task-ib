import authService.AuthUser;
import db.DB;
import entity.User;
import enums.Role;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AuthUserTest {

    @Test
    void testPromoteUser() throws InterruptedException {
        User testUser = new User("Test", "test@example.com", "password");
        testUser.setRole(Role.User);

        AuthUser.currentUser = testUser;
        boolean result = AuthUser.promote();

        assertTrue(result);
        assertEquals(Role.User, testUser.getRole());
        assertEquals(1, DB.REQUESTED_USERS.size());
    }

    @Test
    void testPromoteAdmin() throws InterruptedException {
        User testAdmin = new User("Admin", "admin@example.com", "adminpass");
        testAdmin.setRole(Role.Admin);
        DB.USERS.add(testAdmin);

        AuthUser.currentUser = testAdmin;
        boolean result = AuthUser.promote();

        assertFalse(result);
        assertEquals(Role.Admin, testAdmin.getRole());
        assertEquals(0, DB.REQUESTED_USERS.size());
    }

    @Test
    void testAuthenticateLogin() throws InterruptedException {
        User testUser = new User("Test", "test@example.com", "password");
        DB.USERS.add(testUser);

        AuthUser.currentUser = null;
        AuthUser.authenticate();

        assertNotNull(AuthUser.currentUser);
        assertEquals("test@example.com", AuthUser.currentUser.getEmail());
    }

    @Test
    void testAuthenticateInvalidLogin() throws InterruptedException {
        AuthUser.currentUser = null;
        AuthUser.authenticate();

        assertNull(AuthUser.currentUser);
    }

    @Test
    void testRegisterUser() {
        TestInput.provideInput("Test\nuser@example.com\npassword\n");

        AuthUser.registerUser();

        assertEquals(1, DB.USERS.size());
        assertEquals("Test", DB.USERS.get(0).getName());
    }

}
